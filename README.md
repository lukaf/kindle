### rfc.awk

An ugly awk script that _converts_ plain text RFC to HTML for use with kindlegen.

        $ curl -O http://www.ietf.org/rfc/rfc2616.txt
        $ awk -f rfc.awk rfc2616.txt > rfc2616.html

At this point you could edit the generated HTML in order to change the title or
similar stuff.

        $ ./kindlegen rfc2616.html

And move the created mobi file to your Kindle.

[kindlegen](http://www.amazon.com/gp/feature.html?ie=UTF8&docId=1000765211)
