BEGIN {
    empty = 0;
    max_allowed_empty = 1;
    keyword_length = 25
    paragraph_title_length = 55;

    # header
    print "<html><head><title>test</title></head><body>";
}

!/^RFC.*/ && !/.*\[Page [0-9]+\]$/ && !// {
    # remove duplicated empty lines
    if (NF == 0) {
        empty++
        if (empty > max_allowed_empty) {
            next;
        }
    } else {
        empty = 0;
    }

    # URL encode
    gsub(/&/, "\\&amp;");
    gsub(/</, "\\&lt;");
    gsub(/>/, "\\&gt;");
    gsub(/"/, "\\&quot;");

    gsub(/^[ ]{3}/, "");

    # paragraph titles
    # this completely! destroys TOC
    if (length($0) <= paragraph_title_length) {
        gsub(/(^[0-9.]+\s+[0-9A-Za-z]+[^.]+$)/, "<h4>&</h4>");
    }

    gsub(/[.]{3,}/, "... ");

    # keywords
    if (length($0) != 0 && length($0) <= keyword_length && \
        substr($0, 1, 1) != "<" && substr($0, length($0), 1) != "." && \
        substr($0, 1, 1) != "\"") {
        gsub(/(^[a-zA-Z"*([#";].*[^.:]$)/, "<em>&</em><br />");
    }

    # replace all empty lines with double line break
    gsub(/^$/, "<br /><br />");

    print;
}

END {
    print "</body></html>";
}
